from django.urls import path
from . import views

app_name = 'PPW'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('organization/', views.more, name='more'),
]
